//
//  UIImage+AKWImagings.h
//  FaceSong
//
//  Created by toon on 6/5/14.
//  Copyright (c) 2014 arkorwan. All rights reserved.
//

@interface UIImage (AKWImagings)

- (UIImage *)imageWithOrientation: (UIImageOrientation) orientation;
- (UIImage *)imageAtRect:(CGRect)rect;
- (UIImage *)imageByScalingAspectFillSize:(CGSize)targetSize;
- (UIImage *)imageByScalingAspectFitSize:(CGSize)targetSize;
- (UIImage *)imageByScalingToFillSize:(CGSize)targetSize;
- (UIImage *)imageRotatedByRadians:(CGFloat)radians;
- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees;
- (UIImage *)imageFlippedHorizontal;
- (UIImage *)imageFlippedVertical;
- (UIImage *)grayScaleImage;

@end
