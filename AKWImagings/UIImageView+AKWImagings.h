//
//  UIImageView+AKWImagings.h
//  FaceSing
//
//  Created by toon on 5/31/14.
//  Copyright (c) 2014 arkorwan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (AKWImagings)

-(UIImage *) visibleImage;

@end
