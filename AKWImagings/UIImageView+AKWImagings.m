//
//  UIImageView+AKWImagings.m
//  FaceSing
//
//  Created by toon on 5/31/14.
//  Copyright (c) 2014 arkorwan. All rights reserved.
//

#import "UIImageView+AKWImagings.h"

@implementation UIImageView (AKWImagings)

-(UIImage *) visibleImage
{
    UIGraphicsBeginImageContext(self.frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.layer renderInContext:context];
    UIImage *image =  UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
