//
//  UIImage+AKWImagings.m
//  FaceSong
//
//  Created by toon on 6/5/14.
//  Copyright (c) 2014 arkorwan. All rights reserved.
//

#import "UIImage+AKWImagings.h"

@implementation UIImage (AKWImagings)

-(UIImage *) imageWithOrientation: (UIImageOrientation) orientation
{
    return [[UIImage alloc] initWithCGImage: self.CGImage scale:self.scale orientation:orientation];
}

-(UIImage *) imageAtRect:(CGRect)rect
{
    rect = CGRectMake(rect.origin.x*self.scale, rect.origin.y*self.scale, rect.size.width*self.scale, rect.size.height*self.scale);
    CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:self.scale orientation:self.imageOrientation];
    CGImageRelease(imageRef);
    return result;
    
}

-(CGRect) rectForScalingProportionallyToSize:(CGSize) targetSize useMaxScale:(BOOL) useMaxScale
{
    CGRect result = (CGRect) {{0, 0}, targetSize};
    
    if(!CGSizeEqualToSize(self.size, targetSize)) {
        CGFloat widthFactor = targetSize.width / self.size.width;
        CGFloat heightFactor = targetSize.height / self.size.height;
        CGFloat scaleFactor = useMaxScale ? MAX(widthFactor, heightFactor): MIN(widthFactor, heightFactor);
        result.size.width = self.size.width * scaleFactor;
        result.size.height = self.size.height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor) {
            result.origin.y = (targetSize.height - result.size.height) * 0.5;
        } else if (widthFactor < heightFactor) {
            result.origin.x = (targetSize.width - result.size.width) * 0.5;
        }
    }
    return result;
}

-(UIImage *) imageByScalingToSize:(CGSize) targetSize atRect:(CGRect) thumbnailRect
{
    UIGraphicsBeginImageContext(targetSize);
    [self drawInRect:thumbnailRect];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    if(newImage == nil) NSLog(@"could not scale image");
    return newImage ;
}

-(UIImage *) imageByScalingAspectFillSize:(CGSize)targetSize {
    CGRect targetRect = [self rectForScalingProportionallyToSize:targetSize useMaxScale:YES];
    return [self imageByScalingToSize:targetSize atRect:targetRect];
}


-(UIImage *) imageByScalingAspectFitSize:(CGSize)targetSize {
    CGRect targetRect = [self rectForScalingProportionallyToSize:targetSize useMaxScale:NO];
    return [self imageByScalingToSize:targetSize atRect:targetRect];
}


-(UIImage *) imageByScalingToFillSize:(CGSize)targetSize {
    CGRect targetRect = (CGRect) {{0, 0}, targetSize};
    return [self imageByScalingToSize:targetSize atRect:targetRect];
}


-(UIImage *) imageRotatedByRadians:(CGFloat)radians
{
    CGAffineTransform xfrm = CGAffineTransformMakeRotation(radians);
    CGRect boundingRect = CGRectApplyAffineTransform((CGRect){{0, 0}, self.size}, xfrm);
    CGSize rotatedSize = boundingRect.size;
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    // Rotate the image context
    CGContextRotateCTM(bitmap, radians);
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-self.size.width / 2, -self.size.height / 2, self.size.width, self.size.height), self.CGImage);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(UIImage *) imageRotatedByDegrees:(CGFloat)degrees
{
    return [self imageRotatedByRadians:degrees * M_PI / 180];
}

-(UIImage *) imageFlippedHorizontal
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(context, 0, self.size.height);
    CGContextScaleCTM(context, 1.0f, -1.0f);
    
    CGContextTranslateCTM(context, self.size.width, 0);
    CGContextScaleCTM(context, -1.0f, 1.0f);
    CGContextDrawImage(context, CGRectMake(0.0, 0.0, self.size.width, self.size.height), [self CGImage]);
    
    UIImage *flipedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return flipedImage;
}

-(UIImage *) imageFlippedVertical
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawImage(context, CGRectMake(0.0, 0.0, self.size.width, self.size.height), [self CGImage]);
    
    UIImage *flipedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return flipedImage;
}

-(UIImage *) grayScaleImage
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    CGRect imageRect = (CGRect){{0, 0}, self.size};
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    // Draw a white background
    CGContextSetRGBFillColor(ctx, 1.0f, 1.0f, 1.0f, 1.0f);
    CGContextFillRect(ctx, imageRect);
    
    // Draw the luminosity on top of the white background to get grayscale
    [self drawInRect:imageRect blendMode:kCGBlendModeLuminosity alpha:1.0f];
    
    // Apply the source image's alpha
    [self drawInRect:imageRect blendMode:kCGBlendModeDestinationIn alpha:0.58f];
    
    UIImage* grayscaleImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return grayscaleImage;
}

@end
