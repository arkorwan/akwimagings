Pod::Spec.new do |s|
  s.name     = 'AKWImagings'
  s.version  = '1.1.0'
  s.platform = :ios, '6.0'
  s.license  = 'MIT' 
  s.summary  = 'Simple image processing categories'
  s.homepage = 'https://bitbucket.org/arkorwan/akwimagings'
  s.author   = { 'Worakarn Isaratham' => 'arkorwan@gmail.com' }
  s.source   = { :git => 'https://bitbucket.org/arkorwan/akwimagings.git', :tag => 'v1.1.0' }
  s.description = 'Simple image processing categories.'
  s.source_files = 'AKWImagings/*.{h,m}'
  s.requires_arc = true
end
